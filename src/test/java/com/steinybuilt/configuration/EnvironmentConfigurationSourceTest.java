/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

public class EnvironmentConfigurationSourceTest {

	private final TestSetting matchSetting = new TestSetting("a.b.c", "a.b.c", "matchValue");
	private final TestSetting convertedSetting = new TestSetting("x.y.z", "X_Y_Z", "convertedValue");
	private EnvironmentConfigurationSource source;

	@BeforeClass
	public void beforeClass() {
		this.source = new EnvironmentConfigurationSource();

		Map<String, String> env = System.getenv();
		HashMap<String, Setting> settings = new HashMap<>(env.size() + 2);
		env.forEach((key, value) -> settings.put(key, new Setting(this.source, key, value)));

		settings.put(matchSetting.envName, new Setting(this.source, matchSetting.envName, matchSetting.value));
		settings.put(convertedSetting.envName, new Setting(this.source, convertedSetting.envName, convertedSetting.value));

		this.source.setSettings(settings);
	}

	@Test
	public void testGet_Match() {
		Setting setting = source.get(matchSetting.name);
		assertNotNull(setting);
		assertEquals(setting.getValue(), matchSetting.value);
	}

	@Test
	public void testGet_Converted() {
		Setting setting = source.get(convertedSetting.name);
		assertNotNull(setting);
		assertEquals(setting.getValue(), convertedSetting.value);
	}


	class TestSetting {

		String name;
		String envName;
		String value;

		public TestSetting(String name, String envName, String value) {
			this.name = name;
			this.envName = envName;
			this.value = value;
		}

	}
}
