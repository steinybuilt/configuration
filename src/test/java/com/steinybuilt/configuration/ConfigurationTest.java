/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

import static org.testng.Assert.*;

public class ConfigurationTest {

	private String integralKey = "integral", floatingKey = "floating";
	private long integralValue = 123L, integralDefault = 987L;
	private double floatingValue = 12.3, floatingDefault = 98.7;
	private Configuration primitiveConfig = new Configuration(
			new TestConfigurationSource(integralKey, String.valueOf(integralValue))
					.put(floatingKey, String.valueOf(floatingValue))
	);

	private String getURLKey = "key", getURLValue = "http://www.example.com";
	private Configuration getURLConfig = new Configuration(new TestConfigurationSource(getURLKey, getURLValue));

	private String getFileKey = "key", getFileValue = "app.properties";
	private Configuration getFileConfig = new Configuration(new TestConfigurationSource(getFileKey, getFileValue));

	private String getInetKey = "key", getInetValue = "9.9.9.9";
	private Configuration getInetConfig = new Configuration(new TestConfigurationSource(getInetKey, getInetValue));

	@BeforeClass
	public void beforeClass() throws IOException {
		primitiveConfig.load();
		getURLConfig.load();
		getFileConfig.load();
		getInetConfig.load();
	}

	@Test
	public void testDefaultConfiguration() {
		Configuration config = new Configuration();
		assertEquals(config.getConfigurationSources().size(), 2);
	}

	@Test
	public void testConfigurationList() throws IOException {
	    EnvironmentConfigurationSource source = new EnvironmentConfigurationSource();
		List<ConfigurationSource> sources = Collections.singletonList(source);
		Configuration config = new Configuration(sources).load();
		assertEquals(config.getConfigurationSources().size(), 1);
		assertSame(config.getConfigurationSources().get(0), source);
	}

	@Test(expectedExceptions = {UnsupportedOperationException.class})
	public void testImmutable() {
		new Configuration().getConfigurationSources().add(null);
	}

	@Test
	public void testGet() throws IOException {
		String key = "key";
		String value = "value";
		TestConfigurationSource source = new TestConfigurationSource(key, value);
		Configuration config = new Configuration(source).load();
		Setting setting = config.get(key);
		assertNotNull(setting);
		assertSame(setting.getSource(), source);
		assertSame(setting.getValue(), value);
	}

	@Test
	public void testGetString() throws IOException {
		String key = "key";
		String value = "value";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertSame(config.getString(key), value);
	}

	@Test
	public void testGetString_Miss() throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource("", "")).load();
		assertNull(config.getString("miss"));
	}

	@Test
	public void testGetString_Default() throws IOException {
		String key = "key";
		String value = "value";
		String defaultValue = "defaultValue";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertSame(config.getString(key, defaultValue), value);
	}

	@Test
	public void testGetString_Default_Miss() throws IOException {
		String defaultValue = "defaultValue";
		Configuration config = new Configuration(new TestConfigurationSource("", "")).load();
		assertSame(config.getString("miss", defaultValue), defaultValue);
	}

	@DataProvider
	public Object[][] stringArrays() {
		String[] singleString = {"ab"};
		String[] multiStrings = {"ab", "cd", "ef"};
		return new Object[][] {
				{"ab", singleString}, // 0
				{",ab", singleString},
				{"ab,", singleString},
				{" , ab,", singleString},
				{"ab,cd,ef", multiStrings},
				{" ab,cd,ef", multiStrings}, // 5
				{"ab ,cd,ef", multiStrings},
				{"ab, cd,ef", multiStrings},
				{"ab,cd ,ef", multiStrings},
				{"ab,cd, ef", multiStrings},
				{"ab,cd,ef ", multiStrings}, // 10
				{" ab ,cd,ef", multiStrings},
				{"ab, cd ,ef", multiStrings},
				{"ab,cd, ef ", multiStrings},
				{",ab,cd,ef", multiStrings},
				{"ab,,cd,ef", multiStrings}, // 15
				{"ab,cd,,ef", multiStrings},
				{"ab,cd,ef,", multiStrings},
				{",,ab,cd,ef", multiStrings},
				{"ab,cd,ef,,", multiStrings},
				{",,  ,  ab ,cd, ,, , ,ef  ,,,  ", multiStrings}, // 20
				{"", new String[0]},
				{"   ", new String[0]}
		};
	}

	@Test(dataProvider = "stringArrays")
	public void testGetStringArray(String value, String[] expected) throws IOException {
		String key = "key";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getStringArray(key), expected);
	}

	@Test
	public void testGetStringArray_Miss() throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource("", "")).load();
	    assertNull(config.getStringArray("miss"));
	}

	@DataProvider
	public Object[][] booleans() throws IOException {
		String trueKey = "trueKey", yesKey = "yesKey", onKey = "onKey";
		String trueCaseKey = "trueCaseKey", yesCaseKey = "yesCaseKey", onCaseKey = "onCaseKey";
		Configuration config = new Configuration(
				new TestConfigurationSource(trueKey, "true").put(yesKey, "yes").put(onKey, "on")
				.put(trueCaseKey, "TruE").put(yesCaseKey, "YeS").put(onCaseKey, "oN")
		).load();
		return new Object[][] {
				{config, trueKey},
				{config, yesKey},
				{config, onKey},
				{config, trueCaseKey},
				{config, yesCaseKey},
				{config, onCaseKey}
		};
	}

	@Test(dataProvider = "booleans")
	public void testGetBoolean(Configuration config, String key) {
		assertTrue(config.getBoolean(key));
	}

	@Test
	public void testGetBoolean_Miss() throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource("", "")).load();
		assertNull(config.getBoolean("miss"));
	}

	@Test(dataProvider = "booleans")
	public void testGetBoolean_Default(Configuration config, String key) {
		assertTrue(config.getBoolean(key, false));
	}

	@Test
	public void testGetBoolean_Default_Miss() throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource("", "")).load();
		assertEquals(config.getBoolean("miss", false), false);
	}

	@Test
	public void testGetBooleanArray() throws IOException {
		String key = "key";
		String value = "true, yes, on, false";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getBooleanArray(key), new Boolean[]{true, true, true, false});
	}

	interface ConfigurationGetter extends BiFunction<Configuration, String, Number> {}

	@DataProvider
	public Object[][] primitiveWrappers() {
		return new Object[][] {
				{"key", 123, (ConfigurationGetter)Configuration::getInteger},
				{"key", 123L, (ConfigurationGetter)Configuration::getLong},
				{"key", 12.3F, (ConfigurationGetter)Configuration::getFloat},
				{"key", 12.3D, (ConfigurationGetter)Configuration::getDouble}
		};
	}

	@Test(dataProvider = "primitiveWrappers")
	public void testGetPrimitiveWrapper_Hit(String key, Number value, ConfigurationGetter getter)
			throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource(key, value.toString())).load();
		Number actual = getter.apply(config, key);
		assertEquals(actual.getClass(), value.getClass());
		assertEquals(actual, value);
	}

	@Test(dataProvider = "primitiveWrappers")
	public void testGetPrimitiveWrapper_Miss(String key, Number value, ConfigurationGetter getter)
			throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource(key, value.toString())).load();
		assertNull(getter.apply(config, key + key));
	}

	@Test
	public void testGetInteger_Hit() {
		assertEquals(primitiveConfig.getInteger(integralKey, (int)integralDefault), (int)integralValue);
	}

	@Test
	public void testGetInteger_Miss() {
		assertEquals(primitiveConfig.getInteger("miss", (int)integralDefault), (int)integralDefault);
	}

	@Test
	public void testGetIntegerArray() throws IOException {
		String key = "key";
		String value = "1, 2, 3";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getIntegerArray(key), new Integer[]{1, 2, 3});
	}

	@Test
	public void testGetIntegerArray_Miss() throws IOException {
		Configuration config = new Configuration(new TestConfigurationSource("", "")).load();
		assertNull(config.getIntegerArray("miss"));
	}

	@Test
	public void testGetLong_Hit() {
		assertEquals(primitiveConfig.getLong(integralKey, integralDefault), integralValue);
	}

	@Test
	public void testGetLong_Miss() {
		assertEquals(primitiveConfig.getLong("miss", integralDefault), integralDefault);
	}

	@Test
	public void testGetLongArray() throws IOException {
		String key = "key";
		String value = "1, 2, 3";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getLongArray(key), new Long[]{1L, 2L, 3L});
	}

	@Test
	public void testGetFloat_Hit() {
		assertEquals(primitiveConfig.getFloat(floatingKey, (float)floatingDefault), (float)floatingValue);
	}

	@Test
	public void testGetFloat_Miss() {
		assertEquals(primitiveConfig.getFloat("miss", (float)floatingDefault), (float)floatingDefault);
	}

	@Test
	public void testGetFloatArray() throws IOException {
		String key = "key";
		String value = "1.1, 2.2, 3.3";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getFloatArray(key), new Float[]{1.1F, 2.2F, 3.3F});
	}

	@Test
	public void testGetDouble_Hit() {
		assertEquals(primitiveConfig.getDouble(floatingKey, floatingDefault), floatingValue);
	}

	@Test
	public void testGetDouble_Miss() {
		assertEquals(primitiveConfig.getDouble("miss", floatingDefault), floatingDefault);
	}

	@Test
	public void testGetDoubleArray() throws IOException {
		String key = "key";
		String value = "1.1, 2.2, 3.3";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getDoubleArray(key), new Double[]{1.1, 2.2, 3.3});
	}

	@Test
	public void testGetURL() throws IOException {
	    assertEquals(getURLConfig.getURL(getURLKey), new URL(getURLValue));
	}

	@Test
	public void testGetURL_Miss() throws IOException {
	    assertNull(getURLConfig.getURL(getURLKey + getURLKey));
	}

	@Test(expectedExceptions = {MalformedURLException.class})
	public void testGetURL_Malformed() throws IOException {
		String key = "key";
		String value = "!@#$%^";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		config.getURL(key);
	}

	@Test
	public void testGetURL_Default() throws IOException {
		URL defaultValue = new URL("http://localhost");
		assertEquals(getURLConfig.getURL(getURLKey, defaultValue), new URL(getURLValue));
	}

	@Test
	public void testGetURL_Default_Miss() throws IOException {
		URL defaultValue = new URL("http://localhost");
	    assertSame(getURLConfig.getURL(getURLKey + getURLKey, defaultValue), defaultValue);
	}

	@Test(expectedExceptions = {MalformedURLException.class})
	public void testGetURL_Default_Malformed() throws IOException {
		String key = "key";
		String value = "!@#$%^";
		URL defaultValue = new URL("http://localhost");
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		config.getURL(key, defaultValue);
	}

	@Test
	public void testGetURLArray() throws IOException {
		String key = "key";
		String url1 = "http://one.com", url2 = "http://two.com:8080", url3 = "http://three.com/path";
		String value = url1 + ", " + url2 + ", " + url3;
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getURLArray(key), new URL[]{new URL(url1), new URL(url2), new URL(url3)});
	}

	@Test(expectedExceptions = {MalformedURLException.class})
	public void testGetURLArray_Malformed() throws IOException {
		String key = "key";
		String url1 = "http://one.com", url2 = "!@#$%^", url3 = "http://three.com/path";
		String value = url1 + ", " + url2 + ", " + url3;
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		config.getURLArray(key);
	}

	@Test
	public void testGetFile() {
	    assertEquals(getFileConfig.getFile(getFileKey), new File(getFileValue));
	}

	@Test
	public void testGetFile_Miss() {
	    assertNull(getFileConfig.getFile(getFileKey + getFileKey));
	}

	@Test
	public void testGetFile_Default() {
	    File defaultValue = new File("foreach1.properties");
	    assertEquals(getFileConfig.getFile(getFileKey, defaultValue), new File(getFileValue));
	}

	@Test
	public void testGetFile_Default_Miss() {
	    File defaultValue = new File("foreach1.properties");
	    assertEquals(getFileConfig.getFile(getFileKey + getFileKey, defaultValue), defaultValue);
	}

	@Test
	public void testGetFileArray() throws Exception {
	    String key = "key";
	    String file1 = "config", file2 = "app.properties", file3 = "foreach1.properties";
	    String value = file1 + ", " + file2 + ", " + file3;
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getFileArray(key), new File[]{new File(file1), new File(file2), new File(file3)});
	}
	
	@Test
	public void testGetInetAddress() throws UnknownHostException {
	    assertEquals(getInetConfig.getInetAddress(getInetKey), InetAddress.getByName(getInetValue));
	}

	@Test
	public void testGetInetAddress_Miss() throws UnknownHostException {
	    assertNull(getInetConfig.getInetAddress(getInetKey + getInetKey));
	}

	@Test(expectedExceptions = UnknownHostException.class)
	public void testGetInetAddress_UnknownHost() throws IOException {
		String key = "key";
		String value = "!@#$%^";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		config.getInetAddress(key);
	}

	@Test
	public void testGetInetAddress_Default() throws UnknownHostException {
		InetAddress defaultValue = InetAddress.getByName("149.112.112.112");
	    assertEquals(getInetConfig.getInetAddress(getInetKey, defaultValue), InetAddress.getByName(getInetValue));
	}

	@Test
	public void testGetInetAddress_Default_Miss() throws UnknownHostException {
		InetAddress defaultValue = InetAddress.getByName("149.112.112.112");
	    assertSame(getInetConfig.getInetAddress(getInetKey + getInetKey, defaultValue), defaultValue);
	}

	@Test(expectedExceptions = UnknownHostException.class)
	public void testGetInetAddress_Default_UnknownHost() throws IOException {
		String key = "key";
		String value = "!@#$%^";
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		InetAddress defaultValue = InetAddress.getByName("149.112.112.112");
		config.getInetAddress(key, defaultValue);
	}

	@Test
	public void testGetInetAddressArray() throws IOException {
		String key = "key";
		String inet1 = "9.9.9.9", inet2 = "149.112.112.112", inet3 = "4.4.4.4";
		String value = inet1 + ", " + inet2 + ", " + inet3;
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		assertEquals(config.getInetAddressArray(key), new InetAddress[]{
				InetAddress.getByName(inet1), InetAddress.getByName(inet2), InetAddress.getByName(inet3)});
	}

	@Test(expectedExceptions = {UnknownHostException.class})
	public void testGetInetAddressArray_UnknownHost() throws IOException {
		String key = "key";
		String inet1 = "9.9.9.9", inet2 = "!@#$%^", inet3 = "4.4.4.4";
		String value = inet1 + ", " + inet2 + ", " + inet3;
		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();
		config.getInetAddressArray(key);
	}

	@Test
	public void testInterpolate() throws IOException {
		String replacementKey = "replacementKey";
		String replacementValue = "replacementValue";
		String originalKey = "originalKey";
		String originalValue = "${" + replacementKey + "}";

		Configuration config = new Configuration(
				new TestConfigurationSource(replacementKey, replacementValue).put(originalKey, originalValue)
		).load();

		assertEquals(replacementValue, config.interpolate(originalValue));
	}

	@Test
	public void testInterpolate_Multiple() throws IOException {
		String oneKey = "one";
		String oneValue = "1";
		String twoKey = "two";
		String twoValue = "2";
		String threeKey = "three";
		String threeValue = "3";
		String eqKey = "equation";
		String eqValue = String.format("${%s} + ${%s} = ${%s}", oneKey, twoKey, threeKey);

		Configuration config = new Configuration(
				new TestConfigurationSource(eqKey, eqValue)
						.put(threeKey, threeValue)
						.put(twoKey, twoValue)
						.put(oneKey, oneValue)
		).load();

		String expected = String.format("%s + %s = %s", oneValue, twoValue, threeValue);
		assertEquals(expected, config.interpolate(eqValue));
	}

	@Test
	public void testInterpolate_Chain() throws IOException {
		String restPortKey = "rest.port";
		String restPortValue = "8080";
		String servicePortKey = "service.port";
		String servicePortValue = "${" + restPortKey + "}";
		String backendPortKey = "backend.port";
		String backendPortValue = "${" + servicePortKey + "}";

		Configuration config = new Configuration(
				new TestConfigurationSource(restPortKey, restPortValue)
						.put(servicePortKey, servicePortValue)
						.put(backendPortKey, backendPortValue)
		).load();

		assertEquals(restPortValue, config.interpolate(backendPortValue));
	}

	@Test
	public void testInterpolate_Miss() throws IOException {
		String key = "test";
		String value = "${miss}";

		Configuration config = new Configuration(new TestConfigurationSource(key, value)).load();

		assertEquals(value, config.interpolate(value));
	}

	@Test
	public void testInterpolate_Setting() throws IOException {
		String replacementKey = "replacementKey";
		String replacementValue = "replacementValue";
		String originalKey = "originalKey";
		String originalValue = "${" + replacementKey + "}";

		Configuration config = new Configuration(
				new TestConfigurationSource(replacementKey, replacementValue).put(originalKey, originalValue)
		).load();

		assertEquals(replacementValue, config.interpolate(config.get(originalKey)).getValue());
	}

	@Test
	public void testForAll() throws IOException {
	    Configuration config = new Configuration(
			    new URLConfigurationSource(getClass().getResource("/foreach1.properties")),
			    new URLConfigurationSource(getClass().getResource("/foreach2.properties"))
	    ).load();

		StringWriter output = new StringWriter();
		PrintWriter writer = new PrintWriter(output);
	    config.forAll(writer::println);

		String[] lines = output.toString().split("\r?\n");
		assertEquals(lines.length, 3);
		assertTrue(Pattern.matches("eh = eh \\(.+\\bforeach1.properties\\)", lines[0]));
		assertTrue(Pattern.matches("eh = error \\(.+\\bforeach2.properties\\)", lines[1]));
		assertTrue(Pattern.matches("bee = bee \\(.+\\bforeach2.properties\\)", lines[2]));
	}

	@Test(enabled = false)
	public void testForEach_Manual() throws IOException {
		Configuration config = new Configuration(
				new EnvironmentConfigurationSource(),
				new SystemPropertiesConfigurationSource(),
				new URLConfigurationSource(getClass().getResource("/foreach1.properties")),
				new URLConfigurationSource(getClass().getResource("/foreach2.properties"))
		).load();
		config.forEach(setting -> System.out.println(config.interpolate(setting)));
	}

	@DataProvider
	public Object[][] iterateFunctions() {
		return new BiConsumer[][] {
				{(BiConsumer<Configuration, PrintWriter>)this::testIterable},
				{(BiConsumer<Configuration, PrintWriter>)this::testIterator},
				{(BiConsumer<Configuration, PrintWriter>)this::testForEach},
				{(BiConsumer<Configuration, PrintWriter>)this::testSpliterator},
				{(BiConsumer<Configuration, PrintWriter>)this::testStream},
				{(BiConsumer<Configuration, PrintWriter>)this::testStreamParallel}
		};
	}

	@Test(dataProvider = "iterateFunctions")
	public void testIteration(BiConsumer<Configuration, PrintWriter> iterate) throws IOException {
	    Configuration config = new Configuration(
			    new URLConfigurationSource(getClass().getResource("/foreach1.properties")),
			    new URLConfigurationSource(getClass().getResource("/foreach2.properties"))
	    ).load();

		StringWriter output = new StringWriter();
		PrintWriter writer = new PrintWriter(output);
		iterate.accept(config, writer);

		String[] lines = output.toString().split("\r?\n");
		assertEquals(lines.length, 2);
		assertTrue(Pattern.matches("bee = bee \\(.+\\bforeach2.properties\\)", lines[0]));
		assertTrue(Pattern.matches("eh = eh \\(.+\\bforeach1.properties\\)", lines[1]));
	}

	public void testIterable(Configuration config, PrintWriter writer) {
		for(Setting setting : config) {
			writer.println(setting);
		}
	}

	public void testIterator(Configuration config, PrintWriter writer) {
		Iterator<Setting> iterator = config.iterator();
		//noinspection WhileLoopReplaceableByForEach
		while(iterator.hasNext()) {
			writer.println(iterator.next());
		}
	}

	public void testForEach(Configuration config, PrintWriter writer) {
		config.forEach(writer::println);
	}

	public void testSpliterator(Configuration config, PrintWriter writer) {
		Spliterator<Setting> spliterator = config.spliterator();
		spliterator.forEachRemaining(writer::println);
	}

	public void testStream(Configuration config, PrintWriter writer) {
		StreamSupport.stream(config.spliterator(), false).forEach(writer::println);
	}

	public void testStreamParallel(Configuration config, PrintWriter writer) {
		StreamSupport.stream(config.spliterator(), true).forEachOrdered(writer::println);
	}

	class TestConfigurationSource extends ConfigurationSource {

		private final Map<String, Setting> settings;

		public TestConfigurationSource(String key, String value) {
			this.settings = new HashMap<>(1);
			this.settings.put(key, new Setting(this, key, value));
		}

		public TestConfigurationSource put(String key, String value) {
			settings.put(key, new Setting(this, key, value));
			return this;
		}

		@Override
		public void load() {
			setSettings(settings);
		}

	}

}
