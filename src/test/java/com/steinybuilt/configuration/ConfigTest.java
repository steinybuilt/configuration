/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.*;

public class ConfigTest {

	private Config config;

	@BeforeClass
	public void beforeClass() throws IOException {
		System.setProperty("user.dir", System.getProperty("user.dir") + "/target/test-classes/user_dir");
		this.config = new Config("app").load();
	}

	@DataProvider
	public Object[][] properties() {
		return new String[][] {
				{"one"},
				{"two"},
				{"three"},
				{"four"}
		};
	}

	@Test(dataProvider = "properties")
	public void testConfig(String property) {
		assertEquals(config.getString(property), property);
	}

	@Test
	public void testProfile() throws IOException {
	    Config config = new Config("app", "p1", "p2").load();
	    assertEquals(config.getString("p1"), "p1");
	}

	@Test
	public void testProfileOverDir() throws IOException {
		Config config = new Config("app", "p1", "p2").load();
		assertEquals(config.getString("profile.over.dir"), "p1 overrides p2 in higher priority dir");
	}

}
