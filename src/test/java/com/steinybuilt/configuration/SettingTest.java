/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import org.testng.annotations.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.testng.Assert.*;

public class SettingTest {

	private ConfigurationSource source = new EnvironmentConfigurationSource();
	private String key = "key";
	private String value = "value";
	private String initialValue = "initialValue";
	private Setting presentSetting = new Setting(source, key, value);
	private Setting absentSetting = new Setting(source, key, null);

	@Test
	public void testGet() {
		assertEquals(presentSetting.get(), value);
	}

	@Test
	public void testIfPresent_Present() {
		AtomicReference<String> retrieved = new AtomicReference<>(initialValue);
		presentSetting.ifPresent(retrieved::set);
		assertEquals(retrieved.get(), value);
	}

	@Test
	public void testIfPresent_Absent() {
		AtomicReference<String> retrieved = new AtomicReference<>(initialValue);
		absentSetting.ifPresent(retrieved::set);
		assertEquals(retrieved.get(), initialValue);
	}

	@Test
	public void testIsPresent_Present() {
		assertTrue(presentSetting.isPresent());
	}

	@Test
	public void testIsPresent_Absent() {
		assertFalse(absentSetting.isPresent());
	}

	@Test
	public void testOrElse_Present() {
		assertEquals(presentSetting.orElse("other"), value);
	}

	@Test
	public void testOrElse_Absent() {
		String other = "other";
		assertEquals(absentSetting.orElse(other), other);
	}

	@Test
	public void testGetSource() {
		assertSame(presentSetting.getSource(), source);
	}

	@Test
	public void testGetKey() {
		assertEquals(presentSetting.getKey(), key);
	}

	@Test
	public void testGetValue_Present() {
		assertEquals(presentSetting.getValue(), value);
	}

	@Test
	public void testGetValue_Absent() {
		assertNull(absentSetting.getValue());
	}

}
