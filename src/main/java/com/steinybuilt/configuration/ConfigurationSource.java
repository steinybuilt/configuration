/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;

public abstract class ConfigurationSource {

	private Map<String, Setting> settings;

	public abstract void load() throws IOException;

	protected void loadFromInputStream(InputStream inputStream) throws IOException {
		Properties props = new Properties();
		props.load(new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)));

		HashMap<String, Setting> settings = new HashMap<>(props.size());
		props.forEach((key, value) ->
				settings.put((String)key, new Setting(this, (String)key, (String)value)));

		setSettings(settings);
	}

	protected synchronized void setSettings(Map<String, Setting> settings) {
		this.settings = settings;
	}

	public synchronized Setting get(String key) {
		return settings.get(key);
	}

	public void forEach(Consumer<String> action) {
		settings.keySet().forEach(action);
	}

	@Override
	public String toString() {
		return getClass().getCanonicalName();
	}

}
