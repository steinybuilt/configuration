/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import java.util.HashMap;
import java.util.Map;

public class EnvironmentConfigurationSource extends ConfigurationSource {

	@Override
	public void load() {
		Map<String, String> env = System.getenv();

		HashMap<String, Setting> settings = new HashMap<>(env.size());
		env.forEach((key, value) -> settings.put(key, new Setting(this, key, value)));

		setSettings(settings);
	}

	@Override
	public Setting get(String key) {
		Setting setting = super.get(key);
		if(setting != null) {
			return setting;
		}

		char[] envKey = new char[key.length()];
		for(int i = 0 ; i < key.length() ; i++) {
			char c = key.charAt(i);
			if(Character.isLetterOrDigit(c)) {
				envKey[i] = Character.toUpperCase(c);
			} else {
				envKey[i] = '_';
			}
		}

		return super.get(new String(envKey));
	}

	@Override
	public String toString() {
		return "Environment";
	}

}
