/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Config extends Configuration {

	public Config(String baseName, String... profiles) {
		Objects.requireNonNull(baseName);
		if(profiles == null) {
			profiles = new String[0];
		}

		ArrayList<ConfigurationSource> sources = new ArrayList<>(profiles.length * 4 + 5);

		sources.add(new EnvironmentConfigurationSource());
		sources.add(new SystemPropertiesConfigurationSource());

		// Scan the filesystem

		Optional<File> pWorking = checkDir(System.getProperty("user.dir"));
		if(pWorking.isPresent()) {
			File working = pWorking.get();

			File configDir = new File(working, "config");
			for(String profile : profiles) {
				if(configDir.exists()) {
					checkProfileFile(configDir, baseName, profile).ifPresent(sources::add);
				}
				checkProfileFile(working, baseName, profile).ifPresent(sources::add);
			}

			if(configDir.exists()) {
				checkFile(configDir, baseName).ifPresent(sources::add);
			}

			checkFile(working, baseName).ifPresent(sources::add);
		}

		// Scan the classpath

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String configBaseName = "config/" + baseName;

		for(String profile : profiles) {
			sources.addAll(checkProfileResource(classLoader, configBaseName, profile));
			sources.addAll(checkProfileResource(classLoader, baseName, profile));
		}

		sources.addAll(checkResource(classLoader, configBaseName));
		sources.addAll(checkResource(classLoader, baseName));

		setConfigurationSources(sources);
	}

	@Override
	public Config load() throws IOException {
		super.load();
		return this;
	}

	protected Optional<File> checkDir(String name) {
		File dir;
		if(name != null && (dir = new File(name)).exists()) {
			return Optional.of(dir);
		}
		return Optional.empty();
	}

	protected Optional<URLConfigurationSource> checkProfileFile(File configDir, String baseName, String profile) {
		return checkFile(configDir, genProfileBaseName(baseName, profile));
	}

	protected Optional<URLConfigurationSource> checkFile(File configDir, String baseName) {
		File file = new File(configDir, genFileName(baseName));
		if(file.exists()) {
			try {
				return Optional.of(new URLConfigurationSource(new URL("file", null, file.getPath())));
			} catch(MalformedURLException ignored) {
			}
		}
		return Optional.empty();
	}

	protected List<URLConfigurationSource> checkProfileResource(ClassLoader classLoader, String baseName, String profile) {
		return checkResource(classLoader, genProfileBaseName(baseName, profile));
	}

	protected List<URLConfigurationSource> checkResource(ClassLoader classLoader, String baseName) {
		ArrayList<URLConfigurationSource> sources = new ArrayList<>();
		try {
			Enumeration<URL> resources = classLoader.getResources(genFileName(baseName));
			while(resources.hasMoreElements()) {
				URL base = resources.nextElement();
				sources.add(new URLConfigurationSource(base));
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		return sources;
	}

	protected String genProfileBaseName(String baseName, String profileName) {
		return baseName + '-' + profileName;
	}

	protected String genFileName(String baseName) {
		return baseName + ".properties";
	}

}
