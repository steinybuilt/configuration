/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Setting implements Supplier<String> {

	private final ConfigurationSource source;
	private final String key;
	private final String value;

	public Setting(ConfigurationSource source, String key, String value) {
		this.source = source;
		this.key = key;
		this.value = value;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) {
			return true;
		}
		if(!(o instanceof Setting)) {
			return false;
		}
		Setting setting = (Setting)o;
		return Objects.equals(getKey(), setting.getKey()) &&
				Objects.equals(getValue(), setting.getValue());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getKey(), getValue());
	}

	@Override
	public String toString() {
		return getKey() + " = " + getValue() + " (" + getSource() + ')';
	}

	@Override
	public String get() {
		return getValue();
	}

	public void ifPresent(Consumer<String> consumer) {
		if(isPresent()) {
			consumer.accept(getValue());
		}
	}

	public boolean isPresent() {
		return getValue() != null;
	}

	public String orElse(String other) {
		return (isPresent() ? getValue() : other);
	}

	public ConfigurationSource getSource() {
		return source;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
