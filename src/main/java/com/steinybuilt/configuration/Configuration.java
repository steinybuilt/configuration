/*
 * Copyright 2018 Chris Steinhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.steinybuilt.configuration;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Configuration implements Iterable<Setting> {

	protected Pattern variablePattern = Pattern.compile("\\$\\{([^}]+)}"); // ${variable}
	protected Pattern arraySplit = Pattern.compile("\\s*(?:,\\s*)+"); // ignores blank elements
	private List<ConfigurationSource> sources;

	public Configuration() {
		this(new EnvironmentConfigurationSource(), new SystemPropertiesConfigurationSource());
	}

	protected Configuration(List<ConfigurationSource> sources) {
		setConfigurationSources(sources);
	}

	protected Configuration(ConfigurationSource... sources) {
		setConfigurationSources(sources);
	}

	public Configuration load() throws IOException {
		for(ConfigurationSource source : sources) {
			source.load();
		}
		return this;
	}

	public List<ConfigurationSource> getConfigurationSources() {
		return sources;
	}

	protected void setConfigurationSources(List<ConfigurationSource> sources) {
		this.sources = Collections.unmodifiableList(new ArrayList<>(sources));
	}

	public void setConfigurationSources(ConfigurationSource... sources) {
		List<ConfigurationSource> tmp = new ArrayList<>(sources.length);
		Collections.addAll(tmp, sources);
		this.sources = Collections.unmodifiableList(tmp);
	}

	public Setting get(String key) {
		List<ConfigurationSource> sources = getConfigurationSources();
		for(ConfigurationSource source : sources) {
			Setting setting = source.get(key);
			if(setting != null) {
				return setting;
			}
		}
		return new Setting(null, key, null);
	}

	public String getString(String key) {
		return interpolate(get(key).getValue());
	}

	public String getString(String key, String defaultValue) {
		String value = interpolate(get(key).getValue());
		return (value == null ? defaultValue : value);
	}

	public String[] getStringArray(String key) {
		String value = getString(key);
		if(value == null) {
			return null;
		}

		value = value.trim();

		if(value.length() == 0) {
			return new String[0];
		}

		if(value.charAt(0) == ',') {
			int i = 1;
			while(value.charAt(i) == ',' || Character.isWhitespace(value.charAt(i))) {
				i++;
			}
			value = value.substring(i);
		}

		return arraySplit.split(value);
	}

	public Boolean getBoolean(String key) {
		String value = getString(key);
		if(value == null) {
			return null;
		}
		value = value.toLowerCase();
		return isBoolean(value);
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		String value = getString(key);
		if(value == null) {
			return defaultValue;
		}
		value = value.toLowerCase();
		return isBoolean(value);
	}

	public Boolean[] getBooleanArray(String key) {
		return getArray(key, Boolean[]::new, this::isBoolean);
	}

	public Integer getInteger(String key) {
		String value = getString(key);
		return (value == null ? null : new Integer(value));
	}

	public int getInteger(String key, int defaultValue) {
		String value = getString(key);
		return (value == null ? defaultValue : Integer.parseInt(value));
	}

	public Integer[] getIntegerArray(String key) {
		return getArray(key, Integer[]::new, Integer::new);
	}

	public Long getLong(String key) {
		String value = getString(key);
		return (value == null ? null : new Long(value));
	}

	public long getLong(String key, long defaultValue) {
		String value = getString(key);
		return (value == null ? defaultValue : Long.parseLong(value));
	}

	public Long[] getLongArray(String key) {
		return getArray(key, Long[]::new, Long::new);
	}

	public Float getFloat(String key) {
		String value = getString(key);
		return (value == null ? null : new Float(value));
	}

	public float getFloat(String key, float defaultValue) {
		String value = getString(key);
		return (value == null ? defaultValue : Float.parseFloat(value));
	}

	public Float[] getFloatArray(String key) {
		return getArray(key, Float[]::new, Float::new);
	}

	public Double getDouble(String key) {
		String value = getString(key);
		return (value == null ? null : new Double(value));
	}

	public double getDouble(String key, double defaultValue) {
		String value = getString(key);
		return (value == null ? defaultValue : Double.parseDouble(value));
	}

	public Double[] getDoubleArray(String key) {
		return getArray(key, Double[]::new, Double::new);
	}

	public URL getURL(String key) throws MalformedURLException {
		String value = getString(key);
		return (value == null ? null : new URL(value));
	}

	public URL getURL(String key, URL defaultValue) throws MalformedURLException {
		String value = getString(key);
		return (value == null ? defaultValue : new URL(value));
	}

	public URL[] getURLArray(String key) throws MalformedURLException {
		return getArray(key, URL[]::new, URL::new);
	}

	public File getFile(String key) {
		String value = getString(key);
		return (value == null ? null : new File(value));
	}

	public File getFile(String key, File defaultValue) {
		String value = getString(key);
		return (value == null ? defaultValue : new File(value));
	}

	public File[] getFileArray(String key) {
		return getArray(key, File[]::new, File::new);
	}

	public InetAddress getInetAddress(String key) throws UnknownHostException {
		String value = getString(key);
		return (value == null ? null : InetAddress.getByName(value));
	}

	public InetAddress getInetAddress(String key, InetAddress defaultValue) throws UnknownHostException {
		String value = getString(key);
		return (value == null ? defaultValue : InetAddress.getByName(value));
	}

	public InetAddress[] getInetAddressArray(String key) throws UnknownHostException {
		return getArray(key, InetAddress[]::new, InetAddress::getByName);
	}

	public Setting interpolate(Setting setting) {
		return new Setting(setting.getSource(), setting.getKey(), interpolate(setting.getValue()));
	}

	public String interpolate(String value) {
		if(value == null) {
			return null;
		}

		Matcher matcher = variablePattern.matcher(value);
		int start = 0;
		if(matcher.find(start)) {
			StringBuilder buff = new StringBuilder(Math.min(5000, value.length() * 2));

			do {
				buff.append(value.substring(start, matcher.start()));
				String val = getString(matcher.group(1));
				if(val == null) {
					buff.append(matcher.group(0));
				} else {
					buff.append(val);
				}
				start = matcher.end();
			} while(matcher.find(start));

			buff.append(value.substring(start));
			return buff.toString();
		} else {
			return value;
		}
	}

	public void forAll(Consumer<Setting> action) {
		sources.forEach(source -> source.forEach(key -> action.accept(source.get(key))));
	}

	@Override
	public Iterator<Setting> iterator() {
		return new ConfigurationIterator();
	}

	@Override
	public void forEach(Consumer<? super Setting> action) {
		TreeSet<String> keys = new TreeSet<>();
		sources.forEach(source -> source.forEach(keys::add));
		keys.forEach(key -> action.accept(get(key)));
	}

	@Override
	public Spliterator<Setting> spliterator() {
		TreeSet<String> keys = new TreeSet<>();
		sources.forEach(source -> source.forEach(keys::add));
		return new ConfigurationSpliterator(keys.spliterator());
	}

	protected boolean isBoolean(String value) {
		return (value.equals("true") || value.equals("yes") || value.equals("on"));
	}

	protected <T, E extends Exception> T[] getArray(String key, Function<Integer, T[]> newArray,
			ThrowingFunction<String, T, E> converter) throws E {
		String[] value = getStringArray(key);
		if(value == null) {
			return null;
		}

		T[] array = newArray.apply(value.length);
		for(int i = 0 ; i < value.length ; i++) {
			array[i] = converter.apply(value[i]);
		}

		return array;
	}

	protected interface ThrowingFunction<T, R, E extends Exception> {
		R apply(T t) throws E;
	}

	protected class ConfigurationIterator implements Iterator<Setting> {

		private final Iterator<String> keys;

		public ConfigurationIterator() {
			TreeSet<String> keys = new TreeSet<>();
			sources.forEach(source -> source.forEach(keys::add));
			this.keys = keys.iterator();
		}

		@Override
		public boolean hasNext() {
			return keys.hasNext();
		}

		@Override
		public Setting next() {
			return get(keys.next());
		}

	}

	protected class ConfigurationSpliterator implements Spliterator<Setting> {

		private final Spliterator<String> keys;

		public ConfigurationSpliterator(Spliterator<String> keys) {
			this.keys = keys;
		}

		@Override
		public void forEachRemaining(Consumer<? super Setting> action) {
			keys.forEachRemaining(key -> action.accept(get(key)));
		}

		@Override
		public long getExactSizeIfKnown() {
			return keys.getExactSizeIfKnown();
		}

		@Override
		public boolean hasCharacteristics(int characteristics) {
			return keys.hasCharacteristics(characteristics);
		}

		@Override
		public Comparator<? super Setting> getComparator() {
			return null;
		}

		@Override
		public boolean tryAdvance(Consumer<? super Setting> action) {
			return keys.tryAdvance(key -> action.accept(get(key)));
		}

		@Override
		public Spliterator<Setting> trySplit() {
			Spliterator<String> split = keys.trySplit();
			return (split == null ? null : new ConfigurationSpliterator(split));
		}

		@Override
		public long estimateSize() {
			return keys.estimateSize();
		}

		@Override
		public int characteristics() {
			return keys.characteristics();
		}

	}

}
